<?php
App::uses('AppShell', 'Console/Command');
App::uses('AssetManager', 'AssetManager.Lib');

/**
 * BuildTask for Console/cake to generate compiled assets.
 * 
 * @author Jason <jason@notplugged.in>
 * @version 1.2
 */
class BuildTask extends AppShell {

	/**
	 *
	 * @var int Length of file buffer (4KB)
	 * @since 1.1
	 */
	const BUFFER_LENGTH = 4096;

	/**
	 *
	 * @var string Date format for file timestamps.
	 * @since 1.1
	 */
	const DATE_FORMAT = 'YmdHis';

	/**
	 * Execute the Build Task.
	 * 
	 * @since 1.1
	 */
	public function execute() {
		$this->out(__('[AssetManager] Rebuilding assets.'), 1, Shell::NORMAL);
		
		$this->out(__('[AssetManager] Loading asset configuration.'), 1, Shell::VERBOSE);
		
		// Get the assets ready for compilation.
		$assets = AssetManager::load();
		
		foreach ($assets as $type => $collection) {
			$this->out(__('[AssetManager] Processing %s files.', $type), 1, Shell::NORMAL);
			$this->process($type, $collection);
		}
	}

	/**
	 * Process an AssetCollection
	 * 
	 * @param string $type
	 *        	Asset type for collection
	 * @param AssetCollection $assets
	 *        	AssetCollection to process
	 * @since 1.1
	 */
	protected function process($type, AssetCollection $assets) {
		// Process each module
		foreach ($assets->modules as $name => $module) {
			$this->out(__('[AssetManager::%s] Processing module: %s', $type, $name), 1, Shell::VERBOSE);
			
			// Get a temp file to dump into
			$tempFile = sprintf('%s/AssetManager.%s.%s', TMP, uniqid($name), $type);
			
			$outFile = fopen($tempFile, 'w', false);
			if (!$outFile) {
				$this->err(__('[ERROR] Failed to open temporary file: %s', $tempFile), 1);
				return;
			}
			
			$written = false;
			
			$moduleTime = 0;
			
			$files = $assets->moduleFiles($module);
			
			foreach ($files as $file) {
				if (strtolower(substr($file, 0, 6)) == 'http:/' || strtolower(substr($file, 0, 7)) == 'https:/') {
					$this->out(__('[AssetManager::%s] Skipping external file: %s', $type, $file), 1, Shell::VERBOSE);
					continue;
				} else {
					$this->out(__('[AssetManager::%s] Adding file: %s', $type, $file), 1, Shell::VERBOSE);						
				}
				
				$fileName = WWW_ROOT . DS . $file;
				// Strip out any extra slashes.
				$fileName = str_replace('//', '/', $fileName);
				
				if (!file_exists($fileName)) {
					$this->out(__('[AssetManager::%s] Skipping missing file: %s', $type, $fileName), 1, Shell::NORMAL);
					continue;
				}
				
				// Get the latest mtime of the files for comparison.
				$mtime = filemtime($fileName);
				if ($mtime > $moduleTime) {
					$moduleTime = $mtime;
				}
				
				$inFile = fopen($fileName, 'r', false);
				if (!$inFile) {
					$this->err(__('[ERROR::%s] Failed to open input file: %s', $type, $fileName), 1);
					$this->err(__('[ERROR::%s] Aborting module: %s', $type, $name), 2);
					
					fclose($outFile);
					unlink($tempFile);
					continue 2;
				}
				
				while (!feof($inFile)) {
					$buffer = fread($inFile, self::BUFFER_LENGTH);
					$result = fwrite($outFile, $buffer);
					
					// make sure the write was successful
					if ($result != strlen($buffer)) {
						$this->err(__('[ERROR::%s] Failed to write properly to output file. Aborting module: %', $type, $name), 2);
						
						fclose($inFile);
						fclose($outFile);
						unlink($tempFile);
						continue 3;
					}
				}
				
				$written = true;
				fclose($inFile);
				
				// Add a linefeed at the end of each file, just in case.
				fwrite($outFile, "\n");
			}
			// Close the output file.
			fclose($outFile);
			
			if (!$written) {
				$this->out(__('[AssetManager::%s] Nothing was written.  Skipping module: %s', $type, $name), 2, Shell::VERBOSE);
				// delete the empty file.
				unlink($tempFile);
				continue;
			}
			
			if (date(self::DATE_FORMAT, $moduleTime) > $this->compiledTime($type, $name)) {				
				$this->compile($type, $name, $tempFile);
			} else {
				$this->out(__('[AssetManager::%s] Module is up to date: %s', $type, $name), 3, Shell::VERBOSE);
			}
			
			unlink($tempFile);
		}
	}

	/**
	 * Get the last compile time of a module.
	 * 
	 * @param string $type
	 *        	Asset type
	 * @param string $name
	 *        	Name of module
	 * @return string Timestamp of last compile file or 0.
	 * @since 1.1
	 */
	protected function compiledTime($type, $name) {
		// If the force parameter is used, tell it to compile.
		if ($this->params['force']) {
			return 0;
		}
		
		// Get the latest compiled version of the module:
		$latest = AssetManager::latest($type, $name);
		if ($latest == $name) {
			// Didn't find anything
			return 0;
		}
		
		// retrieve date from it.
		preg_match('/-(?P<date>\d+)\.(?:css|js)$/i', $latest, $matches);
		
		if (!empty($matches['date'])) {
			return $matches['date'];
		}
		
		// return a failure
		return 0;
	}

	/**
	 * Compile/compress a module.
	 * 
	 * @param string $type
	 *        	Asset type
	 * @param string $name
	 *        	Module name
	 * @param string $tempFile
	 *        	Temporary file for module.
	 * @return boolean Success
	 * @since 1.1
	 */
	protected function compile($type, $name, $tempFile) {
		$this->out(__('[AssetManager::%s] Compiling module: %s', $type, $name), 1, Shell::NORMAL);
		
		$compileDirectory = AssetManager::getCompileDirectory($type, true);
		$outputFile = str_replace('//', '/', sprintf('%s/%s-%s.%s', $compileDirectory, $name, date(self::DATE_FORMAT), $type));
		
		$verbose = $this->params['verbose'] ? ' -v' : '';
		
		$vendorPath = App::path('Vendor', 'AssetManager');
		
		// @todo Add a munge/no-munge option
		// Execute the compressor
		$command = sprintf('java -jar %s/yuicompressor.jar%s --nomunge -o %s --type %s %s', $vendorPath[0], $verbose, $outputFile, $type, $tempFile);
		
		exec($command, $output, $return);
		
		$this->out($output, 1, Shell::NORMAL);
		
		if ($return == 0) {
			return true;
		}
		
		return false;
	}
}