<?php
App::uses('AppShell', 'Console/Command');

/**
 * Shell for compiling assets.
 * 
 * @author Jason Burgess <jason@notplugged.in>
 * @version 1.2
 */
class CompileShell extends AppShell {

	/**
	 *
	 * @var string[] Tasks for shell.
	 */
	public $tasks = array (
			'AssetManager.Build');

	/**
	 * Main method.
	 */
	public function main() {
		$this->Build->execute();
	}

	public function getOptionParser() {
		$parser = parent::getOptionParser();
		$parser->addOption('force', array (
				'short' => 'f',
				'help' => __('Force recompile of assets, ignoring last compile time.'),
				'boolean' => true))->description(__('Compile assets for faster performance.'));
		return $parser;
	}
}