<?php
App::import('Lib', 'AssetManager.AssetManager');
App::import('Lib', 'AssetManager.ModuleCollection');
App::import('HtmlHelper', 'Html');

/**
 * Helper for managing styles and scripts. Also provides an easy interface for passing data to and fro via JSON.
 *
 * @todo Fix so this works with media types.
 */
class AssetHelper extends HtmlHelper {

	/**
	 *
	 * @var ModuleCollection Holder for CSS & JavaScript information. Keys are 'css' and 'js'.
	 */
	protected $assets;

	/**
	 *
	 * @var array An array for holding page data to pass via JSON (as needed).
	 */
	protected $pageConfig = array ();

	/**
	 *
	 * @var array AssetHelper settings.
	 */
	public $settings = array ();



	/**
	 *
	 * @var array Extensions used by CSS and JS files.
	 */
	protected $extensions = array (
			'css' => '.css',
			'js' => '.js');

	/**
	 *
	 * @var array List of dependencies to load when loading all.
	 */
	protected $dependencies = array ();

	/**
	 * Perform some initialization of the class.
	 *
	 * @param View $View
	 * @param array $settings
	 * @access public
	 */
	public function __construct(View $View, $settings = array()) {
		parent::__construct($View, $settings);
	}

	/**
	 *
	 * @see Helper::beforeRender()
	 * @access public
	 */
	public function beforeRender($viewFile = false) {
		// Don't start loading assets until here so that we can make sure we're
		// getting the right ones for the action.

		// Build dependencies
		$this->buildDependencies();

		// Load configuration
		$this->loadAssets();
	}

	/**
	 * Outputs the style sheet information.
	 *
	 * @return string Styles and Sheets for layout
	 * @access public
	 * @since 1.5
	 */
	public function fetch($type, $full_paths = false) {
		$output = '';
		$assets = array ();

		if ($type == 'css') {
			// Always add CSS Browser Selector before the stylesheets.
			// Otherwise, the page can jitter.
			$output .= $this->script('/asset_manager/js/css_browser_selector.js', array (
					'inline' => true));
		}

		$mediaList = $this->assets[$type]->mediaList();
		foreach ($mediaList as $media) {
			// Load assets for output;
			$assets = $this->pullAssets($type, $media);

			if ($assets) {
				foreach ($assets as $asset) {
					switch ($type) {
						case AssetManager::ASSET_TYPE_CSS :
							// Pass the media type for CSS
							if ($media != AssetManager::MEDIA_TYPE_ALL) {
								$options = array (
										'media' => $media);
							} else {
								$options = array ();
							}

							$output .= parent::css($asset, $options);
							break;
						case AssetManager::ASSET_TYPE_JS :
							$output .= parent::script($asset);
							break;
					}
				}
			}
		}
		return $output;
	}

	/**
	 * Retrieve assets from the queue.
	 *
	 * @param string $type
	 *        	'css' or 'js'
	 * @param string $queue
	 *        	Queue to pull from. (Default: 'core')
	 * @param bool $full_paths
	 *        	Return full paths to the files. (Default: false)
	 * @return array
	 */
	public function pullAssets($type, $media = AssetManager::MEDIA_TYPE_ALL, $full_paths = false) {
		$output = array ();

		$assets = $this->assets[$type]->getQueue($media);

		if ($assets) {
			// Set base_url from Router.
			if ($full_paths) {
				$base_url = rtrim(Router::url('/', true), '/');
			}

			foreach ($assets as $asset) {
				// Check to see if it's included locally or not.
				if (strtolower(substr($asset, 0, 4)) != 'http') {
					if ($full_paths) {
    					// Prepend the full path, if needed
					    $asset = $base_url . $asset;
					} else {
					    // Otherwise, need to put the root folder back in.
					    $asset = $this->webroot($asset);
					}
				}

				$output[] = $asset;
			}
		}
		var_dump($output);

		return $output;
	}

	/**
	 * Add data for page configuration
	 *
	 * @param array $data
	 *        	new data for pCfg in key => value pairs
	 * @access public
	 * @see Hash::merge()
	 * @since 1.5
	 */
	public function addConfig(array $data) {
		// Use Hash::merge for a more complete merging.
		$this->pageConfig = Hash::merge($this->pageConfig, $data);
	}

	/**
	 * Output page configuration javascript code block
	 *
	 * @return string
	 * @access public
	 * @since 1.5
	 */
	public function showConfig() {
		if ($this->pageConfig) {
			return $this->scriptBlock('var pCfg=' . json_encode($this->pageConfig) . ';', array (
					'allowCache' => false));
		} else {
			return $this->scriptBlock('var pCfg;', array (
					'allowCache' => false));
		}
	}

	/**
	 * For AJAX calls, send anything that would've been a page configuration as a JSON header, including any other json headers as needed.
	 *
	 * @param array $json
	 *        	Additional JSON headers to include, usually from controller.
	 * @access public
	 * @since 2.1
	 */
	public function sendJSON($json = false) {
		// Merge in config as needed
		// @todo see if this is relevant anymore
		if ($this->pageConfig) {
			if (isUpdater()) {
				$json['config'] = $this->pageConfig;
			} else {
				$json = array_merge($json, $this->pageConfig);
			}
		}

		// Send JSON header if we have anything to send.
		if (!empty($json)) {
			$json = 'X-JSON: ' . json_encode($json);
			header($json, true);
		}
	}

	/**
	 * Get JSON data for the view to output.
	 *
	 * @param string $json
	 *        	Additional JSON data to merge with existing data.
	 * @return array JSON data for output.
	 */
	public function getJSON($json = false) {
		// Merge in config as needed
		if ($this->pageConfig) {

			// @todo check if this is necessary for anyone else besides the
			// original project.
			if (isUpdater()) {
				$json['config'] = $this->pageConfig;
			} else {
				$json = array_merge($json, $this->pageConfig);
			}
		}

		return $json;
	}

	/**
	 * Load the assets from app/Config/assets.php
	 */
	protected function loadAssets() {
		$collections = AssetManager::load();

		foreach ($collections as $key => &$collection) {
			// Load modules
			$collection->loadModule('core');

			// Load any external files
			$collection->loadModule('external');

			// Load dependencies
			foreach ($this->dependencies as $d) {
				$collection->loadFor($d);
			}
		}

		$this->assets = $collections;
	}

	/**
	 * Build list of dependencies for loading.
	 */
	protected function buildDependencies() {
		// Always include core.
		$this->dependencies[] = 'core';
		// Load for the controller
		$this->dependencies[] = strtolower($this->params['controller']);
		// Load for the action
		$this->dependencies[] = strtolower(sprintf('%s_%s', $this->params['controller'], $this->params['action']));
		// Load for the theme
		$this->dependencies[] = strtolower(sprintf('theme_%s', $this->_View->theme));
	}
}
