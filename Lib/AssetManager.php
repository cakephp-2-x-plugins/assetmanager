<?php
App::uses('AssetBase', 'AssetManager.Lib');
App::uses('AssetCollection', 'AssetManager.Lib');

App::uses('AssetFile', 'AssetManager.Lib');
App::uses('AssetModule', 'AssetManager.Lib');

App::uses('PhpReader', 'Configure');

// Make sure we can read the configuration files.
if (!Configure::read('default')) {
	// Read config files from app/Config
	Configure::config('default', new PhpReader());
}

/**
 * Load the assets from app/Config/assets.php
 * 
 * @author Jason Burgess <jason@notplugged.in>
 * @version 1.2
 */
class AssetManager extends AssetBase {

	/**
	 * Types of assets to handle.
	 * 
	 * @var string[] Array of asset types.
	 * @access protected
	 * @since 1.1
	 */
	protected static $types = array (
			self::ASSET_TYPE_CSS,
			self::ASSET_TYPE_JS);

	/**
	 * Load assets from configuration file. Stored in app/Config/assets.php
	 * 
	 * @return AssetCollection[] Loaded assets
	 * @since 1.1
	 */
	public static function load() {
		Configure::load('assets', 'default');
		$assets = array ();
		
		$config = Configure::read('Assets');
		if (!empty($config)) {
			foreach (self::$types as $type) {
				// Load files
				if (!empty($config[$type])) {
					$assets[$type] = self::parseType($config[$type], $type);
				}
			}
		}
		
		return $assets;
	}

	/**
	 * Process a type from the loaded asset file.
	 * 
	 * @param array $config
	 *        	Data from file
	 * @param string $type
	 *        	self::ASSET_TYPE_CSS or self::ASSET_TYPE_JS (Default: self::ASSET_TYPE_GENERIC)
	 * @return AssetCollection
	 * @since 1.1
	 */
	protected static function parseType(array $config, $type = self::ASSET_TYPE_GENERIC) {
		$collection = new AssetCollection($type);
		
		// Load files
		if (!empty($config['files'])) {
			foreach ($config['files'] as $key => $file) {
				$collection->addFile(new AssetFile($type, $key, $file));
			}
		}
		
		// Create Modules
		if (!empty($config['modules'])) {
			foreach ($config['modules'] as $name => $module) {
				$collection->addModule(new AssetModule($type, $name, $module));
			}
		}
		
		// Load dependencies
		if (!empty($config['depends'])) {
			foreach ($config['depends'] as $key => $depends) {
				$collection->addDependency($key, $depends);
			}
		}
		
		return $collection;
	}
}