<?php
App::uses('AssetBase', 'AssetManager.Lib');

/**
 * Asset Collection
 * 
 * @author Jason Burgess
 * @version 1.2
 */
class AssetCollection extends AssetBase {

	/**
	 * The type of asset the collection handles.
	 * 
	 * @var string Default: self::ASSET_TYPE_GENERIC
	 */
	public $assetType = self::ASSET_TYPE_GENERIC;

	/**
	 *
	 * @var AssetFile[] Files for assets.
	 */
	public $files = array ();

	/**
	 *
	 * @var AssetModule[] All modules.
	 */
	public $modules = array ();

	/**
	 *
	 * @var string[] Modules to load for specific pages/Controllers.
	 */
	public $dependencies = array ();

	/**
	 * Note: Predefine the all queue so it always loads first.
	 * 
	 * @var string[] Files by media type
	 */
	public $queue = array (
			self::MEDIA_TYPE_ALL => array ());

	/**
	 * Create a new AssetCollection
	 * 
	 * @param string $type
	 *        	Extension for files we're collecting.
	 * @return AssetCollection
	 * @access public
	 * @since 1.0
	 */
	public function __construct($type = self::ASSET_TYPE_GENERIC) {
		$this->assetType = $type;
	}

	/**
	 * Add an AssetFile to the collection.
	 * 
	 * @param AssetFile $file        	
	 * @return AssetFile
	 * @since 1.1
	 */
	public function addFile(AssetFile $file) {
		return $this->files[$file->key] = $file;
	}

	/**
	 * Add modules that load key depends on.
	 * 
	 * @param string $key        	
	 * @param array $depends
	 *        	Module names
	 * @access public
	 * @since 1.1
	 */
	public function addDependency($key, $depends) {
		if (!is_array($depends)) {
			$depends = array (
					$depends);
		}
		
		$this->dependencies[$key] = $depends;
	}

	/**
	 * Add an AssetModule to the collection.
	 * 
	 * @param AssetModule $module
	 *        	AssetModule to add to the collection.
	 * @access public
	 * @since 1.0
	 */
	public function addModule(AssetModule $module) {
		return $this->modules[$module->name] = $module;
	}

	/**
	 * Load depended modules for a key.
	 * 
	 * @param string $key        	
	 * @access public
	 * @since 1.0
	 */
	public function loadFor($key) {
		if (!empty($this->dependencies[$key])) {
			foreach ($this->dependencies[$key] as $dependency) {
				$this->loadModule($dependency);
			}
		}
	}

	/**
	 * Load a module
	 * 
	 * @param unknown_type $name        	
	 * @access public
	 * @since 1.0
	 */
	public function loadModule($name, $media = self::MEDIA_TYPE_ALL) {
		if (empty($this->modules[$name])) {
			// Module isn't defined.
			return false;
		}
		
		if (!Configure::read('debug')) {
			// Get the compile version if it exists.
			$file = $this->latest($this->assetType, $name, false);
		}
		
		// In DEBUG mode, or if the compiled version is not found, include the
		// individual files.
		if (empty($file)) {
			$file = $this->modules[$name]->getFiles($this->files, $this->modules);
		}
		
		if ($file) {
			return $this->queue($file, $media);
		}
		
		return false;
	}

	/**
	 * Get queued output files
	 * 
	 * @return array Files to output
	 * @access public
	 * @since 1.0
	 */
	public function getQueue($name = self::MEDIA_TYPE_ALL) {
		if (empty($this->queue[$name])) {
			// Make sure it isn't empty.
			$this->queue[$name] = array ();
		}
		
		// Filtering for uniqueness here breaks. Should be sufficiently filtered
		// before this point though.
		return $this->queue[$name];
	}

	/**
	 * Get a list of media types used.
	 * 
	 * @return array List of media types.
	 */
	public function mediaList() {
		return array_keys($this->queue);
	}

	/**
	 * Get a list of files for a module.
	 * 
	 * @param AssetModule $module
	 *        	Module to check
	 * @return array File list for module
	 */
	public function moduleFiles(AssetModule $module) {
		return $module->getFiles($this->files, $this->modules);
	}

	/**
	 * Queue a file for use.
	 * 
	 * @param string|string[] $file
	 *        	Filename or array of filenames.
	 * @param string $media
	 *        	Media type
	 * @access protected
	 * @since 1.0
	 */
	protected function queue($file, $media = self::MEDIA_TYPE_ALL) {
		if (empty($this->queue[$media])) {
			// Create the queue if it doesn't exist.
			$this->queue[$media] = array ();
		}
		
		// Allow us to pass an array for convenience.
		if (is_array($file)) {
			$this->queue[$media] = array_merge($this->queue[$media], $file);
		} else {
			$this->queue[$media][] = $file;
		}
		
		return true;
	}
}