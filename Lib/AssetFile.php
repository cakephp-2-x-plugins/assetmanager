<?php
// App::uses('AssetBase', 'AssetManager.Lib');

/**
 * AssetFile contains information about individual asset files.
 * 
 * @author Jason Burgess <jason@notplugged.in>
 * @version 1.2
 * @since 1.1
 */
class AssetFile {

	/**
	 *
	 * @var string File key for module use.
	 * @since 1.1
	 */
	public $key;

	/**
	 *
	 * @var string Asset Type
	 * @since 1.2
	 */
	public $type;

	/**
	 *
	 * @var string Actual file name.
	 * @since 1.1
	 */
	public $filename;

	/**
	 *
	 * @var string[] List of file keys this file depends on.
	 * @since 1.1
	 */
	public $dependencies = array ();

	/**
	 *
	 * @var array Paths to CSS and JS files.
	 * @since 1.2
	 */
	protected $paths = array (
			'css' => CSS_URL,
			'js' => JS_URL);

	/**
	 * Create an instance of AssetFile
	 * 
	 * @param string $type
	 *        	Asset type
	 * @param string $key
	 *        	File key
	 * @param string $filename
	 *        	File name
	 * @since 1.2
	 */
	public function __construct($type, $key, $filename) {
		$this->key = $key;
		$this->type = $type;
		
		// To allow for multiple input methods.
		if (!is_array($filename)) {
			$filename = array (
					'file' => $filename);
		}
		$this->filename = $filename['file'];
		
		// Make sure depends is an array
		if (!empty($filename['depends'])) {
			if (!is_array($filename['depends'])) {
				$this->dependencies = array (
						$filename['depends']);
			} else {
				$this->dependencies = $filename['depends'];
			}
		}
	}

	/**
	 * Get dependencies and file recursively to build a file list.
	 * 
	 * @param AssetFile[] $files
	 *        	AssetFile array to build file list from.
	 * @return string[] Required file names.
	 * @since 1.1
	 */
	public function getFiles($files) {
		$fileList = array ();
		
		// Add it's dependencies first
		if (!empty($this->dependencies)) {
			foreach ($this->dependencies as $dependency) {
				$fileList = array_merge($fileList, $files[$dependency]->getFiles($files));
			}
		}
		
		// Add the file itself
		$fileList[] = $this->fullFilename();
		
		// Strip duplicates
		return array_unique($fileList);
	}

	/**
	 * Process a filename to get the relative path to it.
	 * 
	 * @param string $filename        	
	 */
	public function fullFilename() {
		// Clean the filename of extension for consistency and ease for
		// the user.
		$filename = preg_replace(sprintf('/\.%s$/', $this->type), '', $this->filename);
		
		// Check to see if it's included locally or not.
		if (strtolower(substr($filename, 0, 4)) != 'http') {
			// Check to see if we're coming from the default directory
			// or elsewhere
			if (substr($filename, 0, 1) != '/') {
				// if no slash is included, assume the default directory
				// for the type.
				$filename = $this->paths[$this->type] . $filename;
			} else {
				$filename = substr($filename, 1);
			}
		}
		
		// Append the extension before output. The first extension for
		// the type is the default.
		$filename = sprintf('%s.%s', $filename, $this->type);
		
		return $filename;
	}
}