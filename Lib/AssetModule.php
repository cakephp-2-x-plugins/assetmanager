<?php
App::uses('AssetBase', 'AssetManager.Lib');

/**
 *
 * @author Jason Burgess <jason@notplugged.in>
 * @version 1.2
 */
class AssetModule extends AssetBase {

	/**
	 *
	 * @var string Name of AssetModule
	 */
	public $name;

	/**
	 *
	 * @var string Type of assets
	 */
	public $type;

	/**
	 * Currently, only uses 'exclude' option.
	 * 
	 * @var array Options for module
	 */
	public $options = array ();

	/**
	 *
	 * @var string[] List of file keys for module.
	 */
	public $files = array ();

	/**
	 *
	 * @var string[] Actual list of used files.
	 */
	public $fileList = array ();

	/**
	 *
	 * @var array Default options for AssetModule
	 */
	protected $defaultOptions = array (
			'exclude' => 'core');

	/**
	 * Create an instance of AssetModule
	 * 
	 * @param string $type
	 *        	Asset type (i.e. 'css' or 'js')
	 * @param string $name
	 *        	Name of module
	 * @param string[] $moduleData
	 *        	Files the module uses
	 */
	public function __construct($type, $name, $moduleData) {
		// Pull out any options.
		$options = array ();
		if (!empty($moduleData['options'])) {
			$options = $moduleData['options'];
			unset($moduleData['options']);
		}
		
		// Merge in default options
		$options = array_merge($this->defaultOptions, $options);
		
		// ensure it's an array
		if (!is_array($options['exclude'])) {
			$options['exclude'] = array (
					$options['exclude']);
		}
		
		// Don't exclude ourself
		if (($key = array_search($name, $options['exclude'])) !== false) {
			unset($options['exclude'][$key]);
		}
		
		$this->type = $type;
		$this->name = $name;
		$this->options = $options;
		$this->files = $moduleData;
	}

	/**
	 * Get the actual files needed for the module.
	 * 
	 * @param array $files
	 *        	AssetFile collection
	 * @param array $modules
	 *        	AssetModule collection
	 * @return array Array of files
	 */
	public function getFiles(array $files, array $modules) {
		$fileList = array ();
		
		foreach ($this->files as $key) {
			if (!empty($files[$key])) {
				// Get the files and dependencies.
				$fileList = array_merge($fileList, $files[$key]->getFiles($files));
			}
		}
		
		// Do not sort or the dependencies get out of order, but do strip out
		// duplicates.
		$this->fileList = array_unique($fileList);
		
		$this->processExclusions($modules, $files);
		
		return $this->fileList;
	}

	/**
	 * Strip out excluded files from the fileList.
	 * 
	 * @param array $files
	 *        	AssetFile array
	 * @param array $modules
	 *        	AssetModule array
	 */
	public function processExclusions(array $files, array $modules) {
		if (!empty($this->options['exclude'])) {
			foreach ($this->options['exclude'] as $exclusion) {
				// Make sure the module exists
				if (!empty($modules[$exclusion])) {
					// Get the excluded modules files.
					$excluded = $modules[$exclusion]->getFiles($files, $modules);
					
					$this->fileList = array_diff($this->fileList, $excluded);
				}
			}
		}
	}
}