<?php
App::uses('Configure', 'Core');

/**
 * Base class for AssetManger library classes. Contains several needed definitions and methods.
 * 
 * @author Jason Burgess <jason@notplugged.in>
 * @version 1.2
 */
class AssetBase {

	/**
	 *
	 * @var string Format for the compile directory.
	 * @since 1.1
	 */
	const DEFAULT_COMPILE_DIRECTORY = '/%s/compiled/';

	/**
	 *
	 * @var string CSS asset type
	 * @since 1.1
	 */
	const ASSET_TYPE_CSS = 'css';

	/**
	 *
	 * @var string JavaScript asset type
	 * @since 1.1
	 */
	const ASSET_TYPE_JS = 'js';

	/**
	 *
	 * @var string Generic asset (usually means a failure somewhere)
	 * @since 1.1
	 */
	const ASSET_TYPE_GENERIC = 'generic';

	/**
	 *
	 * @var string Media type for all presentations.
	 * @since 1.1
	 */
	const MEDIA_TYPE_ALL = 'all';

	/**
	 *
	 * @var string Print media type
	 * @since 1.1
	 */
	const MEDIA_TYPE_PRINT = 'print';

	/**
	 *
	 * @var string Mobile/Handheld media type
	 * @since 1.1
	 */
	const MEDIA_TYPE_MOBILE = 'handheld';

	/**
	 *
	 * @var string Screen media type
	 * @since 1.1
	 */
	const MEDIA_TYPE_SCREEN = 'screen';

	/**
	 *
	 * @var string TV media type
	 * @since 1.1
	 */
	const MEDIA_TYPE_TV = 'tv';

	/**
	 * Get the defined compile directory
	 * 
	 * @param string $type
	 *        	If a type is specified, it will automatically be formatted into the result. (Default: false)
	 * @param bool $fullpath
	 *        	If true, returns the full path, instead of the relative path. (Default: false)
	 * @param bool $create
	 *        	If true, the directory will be created if it doesn't exist and a type is specified. (Default: true)
	 * @return string path to /compiled/ relative to webroot
	 * @since 1.1
	 */
	public static function getCompileDirectory($type = self::ASSET_TYPE_GENERIC, $fullPath = false, $create = true) {
		$directory = Configure::read('AssetManager.CompileDirectory');
		if (!$directory) {
			$directory = self::DEFAULT_COMPILE_DIRECTORY;
		}
		
		if ($type) {
			$directory = sprintf($directory, $type);
			
			$fullDirectory = str_replace('//', '/', WWW_ROOT . $directory);
			if (!file_exists($fullDirectory)) {
				mkdir($fullDirectory, 0777, true);
			}
		}
		
		if ($fullPath && !empty($fullDirectory)) {
			return $fullDirectory;
		}
		return $directory;
	}

	/**
	 * Pull the latest compiled version of a file, if possible, or it's source.
	 * 
	 * @param string $type
	 *        	'css' or 'js'
	 * @param string $file
	 *        	Source file to check.
	 * @return string Path to latest version.
	 * @since 1.1
	 */
	public static function latest($type, $file, $returnOriginal = true) {
		// Search pattern for compiled files.
		$pattern = sprintf('/%s-\d+?\.%s/i', basename($file, '.src'), $type);
		
		// check if the file is in the standard directory or not.
		if (substr($file, 0, 1) == '/') {
			$base = WWW_ROOT . dirname($file);
			$returnBase = dirname($file);
		} else {
			$base = self::getCompileDirectory($type, true);
			$returnBase = self::getCompileDirectory($type);
		}
		if (!file_exists($base)) {
			// most likely the compiled directory does not exist.
			return $file;
		}
		$dir = scandir($base, 1);
		
		// Check each possible directory.
		foreach ($dir as $entry) {
			if (preg_match($pattern, $entry)) {
				return $returnBase . '/' . $entry;
			}
		}
		
		if ($returnOriginal) {
			// Return the source if no compiled version was found.
			return $file;
		}
		
		return false;
	}
}